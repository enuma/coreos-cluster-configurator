# create overlay network
docker network create --driver overlay phpnetwork


# run nginx service 
docker service create \
  -d \
  --name nginx \
  --mode global \
  --hostname nginx \
  -p 80:80 \
  --network phpnetwork \
  --constraint 'node.labels.name == global' \
  --mount type=volume,source=nginx-sites,destination=/etc/nginx \
  nginx:1-alpine

# prepare for php-fpm for each project
project_name=supermama
ssh robusta@172.104.132.70 -p 2232 "
  cd /var/www/
  tar -cf $project_name.tar $project_name
"
# run php-fpm service 
docker service create \
  -d \
  --name nginx \
  --mode global \
  --hostname nginx \
  -p 80:80 \
  --network phpnetwork
  --constraint 'node.labels.name == global' \
  --mount type=volume,source=supermama,destination=/var/www/html/ \
  --entrypoint /bin/sh
  php:7.0-fpm-alpine
  -c "
    cd /
    if [ ! -e $project_name.tar ]; then
      wget staging.robustastudio.com/$project_name.tar
      tar -xf $project_name.tar
    fi
    php-fpm
  "
# cleanup
ssh robusta@172.104.132.70 -p 2232 "
  cd /var/www/
  rm $project_name.tar
"