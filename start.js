#!/usr/bin/env node
const request = require('request');
const async = require('async');
const url = require('url');
const fs = require('fs');
const path = require('path');
const node_ssh = require('node-ssh')
const Handlebars = require('handlebars');

const LINODE_API_KEY = process.env.LINODE_API_KEY;
const BASE_URL = 'https://api.linode.com/v4/';

// let ssh = new node_ssh();

const CLUSTER = {
  REGION: 'eu-central-1a',
  DISTRO: 'linode/containerlinux',
  NUMBER: 4,
  TYPE: 'g5-nanode-1',
  NAME: 'cluster-trial',
  SSH_KEY: fs.readFileSync(path.join(process.env.HOME, '/.ssh/id_rsa.pub')).toString().trim(),
  CLOUD_CONFIG_TEMPLATE: Handlebars.compile(fs.readFileSync(path.join(__dirname, 'files/cloud-config.yml')).toString()),
  // PRIVATE_IP_SERVICE_TEMPLATE: Handlebars.compile(fs.readFileSync(path.join(__dirname, 'files/private_ip.service')).toString()),
  // NETWORKD_DROP_IN_TEMPLATE: Handlebars.compile(fs.readFileSync(path.join(__dirname, 'files/networkd-drop-in.conf')).toString()),
  PASSWORD: 'KG_W6H9<eX\]+JwybFctF&>E\&@beGNmtHy5/m^t',
  DISCOVERY_URL: 'https://discovery.etcd.io/fa7a6a11fe39c3073e082f357f3ed009',
  ID_RSA: fs.readFileSync(path.join(__dirname, 'files/id_rsa')).toString(),
  ID_RSA_PUB: fs.readFileSync(path.join(__dirname, 'files/id_rsa.pub')).toString(),
}

// request.debug = true

function makeRequest(method, path, body, filter, callback) {
  let headers = {
    Authorization: `token ${LINODE_API_KEY}`,
  };
  if (Object.keys(filter).length) {
    headers['X-Filter'] = JSON.stringify(filter);
  }
  let qs = {};
  if (method === 'GET') {
    qs = body;
    body = {};
  }
  request({
    url: url.resolve(BASE_URL, path),
    json: true,
    qs,
    body,
    method,
    headers
  }, callback);
}


function getInstanceDetails(label, callback) {
  let filter = {};
  if (label) {
    filter = { label };
  }
  makeRequest('GET', 'linode/instances/', {}, filter, (err, res, data) => {
    if (err) {
      console.error('Error showing instance details');
    }
    // console.log(JSON.stringify(data.linodes[0], null, 2));
    callback(err, data.linodes);
  });
}
//g5-nanode-1
function createInstance(region, type, label, group, distribution, rootPass, rootSSHKey, callback) {
  let data = {
    region,
    type,
    label,
    group,
    distribution,
    root_pass: rootPass,
    root_ssh_key: rootSSHKey,
  }
  // console.log(data);
  makeRequest('POST', 'linode/instances/', data, {}, (err, res, data) => {
    //data for a single linode
    callback(err, data);
  });
}
function addIP(linodeId, type, callback) {
  makeRequest('POST', `linode/instances/${linodeId}/ips`, { type }, {}, (err, res, data) => {
    if (err) {
      console.error('Error adding private IP', err);
    }
    callback(err, data);
  });
}
function updateClusterNodeObject(linode, clusterNode, callback) {
  if (linode) {
    clusterNode.exists = true;
    clusterNode.id = linode.id;
    clusterNode.publicIp = linode.ipv4[0];
    if (linode.ipv4.length === 2) {
      clusterNode.privateIp = linode.ipv4[0];
      clusterNode.publicIp = linode.ipv4[1];
      clusterNode.privateBrodcastIp = clusterNode.privateIp.slice(0, clusterNode.privateIp.lastIndexOf('.')) + '.255';
      clusterNode.publicGateway = clusterNode.publicIp.slice(0, clusterNode.publicIp.lastIndexOf('.')) + '.1';
      clusterNode.privateGateway = clusterNode.privateIp.slice(0, clusterNode.privateIp.lastIndexOf('.')) + '.1';
    }
  }
  // console.log(clusterNode);
  callback(null, clusterNode);
}
function ensurePublicIp(linode, callback) {
  console.log(linode.label, 'Ensure Public IP');
  // if (linode.ipv4.length === 2) {
  //   callback(null, linode);
  // } else {
  addIP(linode.id, 'public', (err, publicIp) => {
    if (!err && publicIp && publicIp.address) {
      linode.ipv4.pop();
      linode.ipv4.push(publicIp.address);
    }
    callback(null, linode)
  });
  // }

}
function ensurePrivateIp(linode, callback) {
  console.log(linode.label, 'Ensure Private IP');
  if (linode.ipv4.length === 2) {
    callback(null, linode);
  } else {
    addIP(linode.id, 'private', (err, privateIp) => {
      linode.ipv4.unshift(privateIp.address)
      callback(err, linode)
    });
  }
}

function ensureNodeExists(clusterNode, callback) {
  console.log(clusterNode.label, 'Ensure node exists');
  async.waterfall([
    nextFunc => getInstanceDetails(clusterNode.label, nextFunc),
    (linodes, nextFunc) => {
      if (linodes.length) {
        nextFunc(null, linodes[0]);
      } else {
        createClusterNode(clusterNode, (err, linode) => {
          // console.log(JSON.stringify(linode, null, 2));
          nextFunc(err, linode);
        });
      }
    },
  ], callback);
}
function bootClusterNode(linode, callback) {
  console.log(linode.label, 'Boot node');
  makeRequest('POST', `linode/instances/${linode.id}/boot`, {}, {}, (err) => {
    if (err) {
      return callback(err);
    }
    // wait for status to be running
    waitTillBoot(linode, callback);
  });
}

function _waitTillBootHelper(linode, callback) {
  getInstanceDetails(linode.label, (err, intervalLinode) => {
    if (intervalLinode[0].status === 'running') {
      // wait for network warmup before trying to connect
      setTimeout(() => {
        console.log('Node is up', linode.label);
        callback(null, linode);
      }, 100 * 1000);
    } else {
      waitTillBoot(linode, callback);
    }
  });
}
function waitTillBoot(linode, callback) {
  setTimeout(_waitTillBootHelper.bind(null, linode, callback), 20 * 1000)
}

/**
 * {"configs": [{"created": "2017-07-09T22:28:43", "helpers": {"enable_distro_helper": false, "enable_modules_dep_helper": false, "enable_network_helper"
: false, "disable_updatedb": false}, "devtmpfs_automount": false, "initrd": null, "ram_limit": 0, "kernel": "linode/direct-disk", "updated": "2017-07-
11T23:52:28", "root_device": "/dev/sda", "id": 4481250, "root_device_ro": true, "run_level": "default", "label": "My CoreOS Container Linux Disk Profi
le", "virt_mode": "paravirt", "comments": "", "disks": {"sda": 8643514, "sdb": 8643515, "sdc": null, "sdd": null, "sde": null, "sdf": null, "sdg": nul
l, "sdh": null}}], "page": 1, "total_pages": 1, "total_results": 1}%
*/
function getLinodeConfig(linode, callback) {
  console.log(linode.label, 'Get Config');
  makeRequest('GET', `linode/instances/${linode.id}/configs`, {}, {}, (err, res, body) => {
    if (err) {
      callback(err);
    } else {
      callback(null, body.configs[0]);
    }
  });
}
function checkBootConfigDifferences(linode, config, nextFunc) {
  let newConfig = {};
  let acceptedConfig = {
    "helpers": {
      "enable_distro_helper": false,
      "enable_modules_dep_helper": false,
      "enable_network_helper": false,
      "disable_updatedb": false
    },
    "kernel": "linode/direct-disk",
  };
  let helpersKeys = Object.keys(config.helpers);
  for (let helperKey of helpersKeys) {
    if (acceptedConfig.helpers[helperKey] !== config.helpers[helperKey]) {
      newConfig.helpers = newConfig.helpers || {};
      newConfig.helpers[helperKey] = acceptedConfig.helpers[helperKey];
    }
  }
  if (acceptedConfig.kernel !== config.kernel) {
    newConfig.kernel = acceptedConfig.kernel;
  }
  nextFunc(null, config, newConfig)
}

function updateBootConfig(linode, config, newConfig, callback) {
  console.log(linode.label, 'Update Boot Config');
  if (Object.keys(newConfig).length) {
    makeRequest('PUT', `linode/instances/${linode.id}/configs/${config.id}`, newConfig, {}, (err, res, body) => {
      callback(err, body);
    });
  } else {
    callback(null, config);
  }
}
function ensureBootConfig(linode, callback) {
  console.log(linode.label, 'Ensure Boot Config');
  async.waterfall([
    nextFunc => getLinodeConfig(linode, nextFunc),
    (config, nextFunc) => checkBootConfigDifferences(linode, config, nextFunc),
    (config, newConfig, nextFunc) => updateBootConfig(linode, config, newConfig, nextFunc),
  ], err => callback(null, linode));
}
function checkIfNodeExists(index, callback) {
  let clusterNode = { name: `coreos-${index + 1}`, label: `coreos-${index + 1}`, exists: false, privateIp: null, publicIp: null };
  console.log(clusterNode.label, 'Boot node');
  async.waterfall([
    nextFunc => ensureNodeExists(clusterNode, nextFunc),
    (linode, nextFunc) => ensurePublicIp(linode, nextFunc),
    (linode, nextFunc) => ensurePrivateIp(linode, nextFunc),
    (linode, nextFunc) => ensureBootConfig(linode, nextFunc),
    (linode, nextFunc) => bootClusterNode(linode, nextFunc),
    (linode, nextFunc) => updateClusterNodeObject(linode, clusterNode, nextFunc),
  ], callback);
}
function ensureClusterNodes(callback) {
  console.log("Ensure Cluster Nodes");
  let nodesList = [];
  if (fs.existsSync('nodes.json')) {
    try {
      nodesList = JSON.parse(fs.readFileSync('nodes.json').toString());
      return callback(null, nodesList);
    } catch (e) {
    }
  }
  async.waterfall([
    nextFunc => async.times(CLUSTER.NUMBER, checkIfNodeExists, nextFunc),
    (nodesList, nextFunc) => saveData('nodes.json', nodesList, nextFunc)
  ], callback);
}
function createClusterNode(clusterNode, callback) {
  createInstance(CLUSTER.REGION, CLUSTER.TYPE, clusterNode.label, CLUSTER.NAME, CLUSTER.DISTRO, CLUSTER.PASSWORD, CLUSTER.SSH_KEY, callback);
}


function saveData(fileName, data, callback) {
  fs.writeFile(fileName, JSON.stringify(data, null, 2), (err) => callback(err, data));
}

function sshConnect(node, ssh, callback) {
  console.log('SSH Connect', node.name)
  ssh.connect({
    host: node.publicIp,
    username: 'core',
    privateKey: path.join(process.env.HOME, '/.ssh/id_rsa')
  })
    .then(() => callback(null))
    .catch(err => {
      if (err.level === 'client-timeout') {
        console.log("retrying to connect to", node.name);
        sshConnect(node, ssh, callback);
      } else {
        callback(err);
      }
    });
}
function createPrivateIpServiceTemplate(node, fileName, callback) {
  fs.writeFile(
    fileName,
    CLUSTER.PRIVATE_IP_SERVICE_TEMPLATE({ node }),
    callback
  )
}
function copyPrivateIpServiceTemplate(node, fileName, callback) {
  ssh.putFile(fileName, '/home/core/private-ip.service')
    .then(() => callback(null))
    .catch(callback);
}
function removePrivateIpServiceTemplate(node, fileName, callback) {
  // fs.unlink(fileName, callback);
  callback(null);
}
function addPrivateIpServiceFile(node, ssh, callback) {
  let fileName = path.join(__dirname, `files/results/${node.name}.private_ip.service`);
  async.series([
    seriesCallback => createPrivateIpServiceTemplate(node, fileName, seriesCallback),
    seriesCallback => copyPrivateIpServiceTemplate(node, fileName, seriesCallback),
    seriesCallback => removePrivateIpServiceTemplate(node, fileName, seriesCallback),
  ], callback);
}

function activatePrivateIpServiceFile(node, ssh, callback) {
  ssh.execCommand(`
    if ! sudo test -f "/etc/systemd/system/private-ip.service"; then
      sudo mv private-ip.service /etc/systemd/system
      sudo systemctl daemon-reload
      sudo systemctl start private-ip.service
    fi
  `, { cwd: '/home/core/' })
    .then(() => callback(null))
    .catch(callback);
}

function createNetwordServiceDropInTemplate(node, fileName, callback) {
  console.log("Create network template", node.name);
  fs.writeFile(fileName, CLUSTER.NETWORKD_DROP_IN_TEMPLATE({ node }), callback);
}
function copyNetwordServiceDropInTemplate(node, ssh, fileName, callback) {
  console.log("Copy network template", node.name);
  ssh.putFile(fileName, '/home/core/networkd-drop-in.conf')
    .then(() => callback(null))
    .catch(callback);
}
function activateNetwordServiceDropIn(node, ssh, destFileName, callback) {
  console.log("Activate network template", node.name);
  ssh.execCommand(`
    sudo mkdir -p /etc/systemd/system/systemd-networkd.service.d
    sudo mv networkd-drop-in.conf ${destFileName}
    sudo systemctl daemon-reload
    sudo systemctl restart systemd-networkd.service
  `, { cwd: '/home/core' })
    .then(() => callback(null))
    .catch(callback);
}
function removeNetwordServiceDropInTemplate(node, fileName, callback) {
  // fs.unlink(fileName, callback);
  console.log("remove network template", node.name);
  callback(null);
}
function modifyNetworkdServiceFile(node, ssh, callback) {
  console.log("Modify Network", node.name);
  // /etc/systemd/system/fleet.service.d/10-restart_60s.conf
  // create a drop-in https://coreos.com/os/docs/latest/using-systemd-drop-in-units.html
  let destFileName = `/etc/systemd/system/systemd-networkd.service.d/10-start_private_ip.conf`
  let fileName = path.join(__dirname, `files/results/${node.name}.networkd-drop-in.conf`);
  async.series([
    seriesCallback => createNetwordServiceDropInTemplate(node, fileName, seriesCallback),
    seriesCallback => copyNetwordServiceDropInTemplate(node, ssh, fileName, seriesCallback),
    seriesCallback => activateNetwordServiceDropIn(node, ssh, destFileName, seriesCallback),
    seriesCallback => removeNetwordServiceDropInTemplate(node, fileName, seriesCallback),
  ], callback);
}
function createCloudConfigTemplate(node, nodesList, fileName, callback) {
  fs.writeFile(fileName, CLUSTER.CLOUD_CONFIG_TEMPLATE({
    node,
    nodesList,
    discovery_url: CLUSTER.DISCOVERY_URL,
    id_rsa: CLUSTER.ID_RSA,
    id_rsa_pub: CLUSTER.ID_RSA_PUB,
  }), callback);
}
function copyCloudConfigTemplate(node, ssh, fileName, callback) {
  ssh.putFile(fileName, '/home/core/cloud-config.yml')
    .then(() => callback(null))
    .catch(callback);
}
function activateCloudConfig(node, ssh, destFileName, callback) {
  ssh.execCommand(`
    sudo mv cloud-config.yml ${destFileName}
  `, { cwd: '/home/core' })
    .then(() => callback(null))
    .catch(callback);
}
function removeCloudConfigTemplate(node, fileName, callback) {
  // fs.unlink(fileName, callback);
  callback(null);
}
function modifyCloudConfigFile(node, nodesList, ssh, callback) {
  console.log("Modify cloud config", node.name);
  let destFileName = `/var/lib/coreos-install/user_data`
  let fileName = path.join(__dirname, `files/results/${node.name}.cloud-config.yml`);
  async.series([
    seriesCallback => createCloudConfigTemplate(node, nodesList, fileName, seriesCallback),
    seriesCallback => copyCloudConfigTemplate(node, ssh, fileName, seriesCallback),
    seriesCallback => activateCloudConfig(node, ssh, destFileName, seriesCallback),
    seriesCallback => removeCloudConfigTemplate(node, fileName, seriesCallback),
  ], callback);
}
// function modifyHostsFile(currentNode, nodes, ssh, callback) {
//   console.log("Hosts File", currentNode.name);
//   let otherNodes = nodes.filter(node => node.id !== currentNode.id)
//     .reduce((acc, node) => acc + node.privateIp + ' ' + node.name + '\n', '');

//   ssh.execCommand(`
//     sudo echo '${otherNodes}' >> /etc/hosts
//     sudo sort -u -o /etc/hosts /etc/hosts
//   `, { cwd: '/home/core' })
//     .then(() => callback(null))
//     .catch(callback);
// }
function rebootClusterNode(node, callback) {
  console.log('Reboot Cluster Node:', node.name);
  makeRequest('POST', `linode/instances/${node.id}/reboot`, {}, {}, callback);
}
function addSSHKey(node, ssh, callback) {
  console.log('Adding ssh key to the cluster nodes');
  let idRSAFileName = path.join(__dirname, `files/id_rsa`);
  let idRSAPubFileName = path.join(__dirname, `files/id_rsa.pub`);
  let idRSADestFileName = '/home/core/.ssh/id_rsa';
  let idRSAPubDestFileName = '/home/core/.ssh/id_rsa.pub';
  Promise.all([
    ssh.putFile(idRSAFileName, idRSADestFileName),
    ssh.putFile(idRSAPubFileName, idRSAPubDestFileName)
  ])
    .then(() => {
      ssh.execCommand(`
        sudo chmod 644 ${idRSAPubDestFileName}
        sudo chmod 600 ${idRSADestFileName}
      `);
      callback(null);
    })
    .catch(callback);
}
function updateNodeCoreOS(node, ssh, callback) {
  console.log('Update cluster node os', node.label);
  ssh.execCommand(`
    sudo systemctl unmask update-engine.service
    sudo systemctl start update-engine.service
    sudo update_engine_client -update
  `).then(() => callback(null))
    .catch(callback);
}
function initTheSwarm(node, callback) {
  let ssh = new node_ssh();
  callback(null);
}
function clearETCD2Directory(node, ssh, callback) {
  console.log('Clearing proxy etcd2 files from node', node.label);
  ssh.execCommand(`
    sudo systemctl stop etcd2
    sudo rm -rf /var/lib/etcd2/*
  `).then(() => callback(null))
    .catch(callback);
}
function execSSHCommands(nodesList, callback) {
  console.log("Exec SSH Commands");
  async.each(nodesList, (node, eachCallback) => {
    let ssh = new node_ssh();
    async.series([
      seriesCallback => sshConnect(node, ssh, seriesCallback),
      // seriesCallback => addSSHKey(node, ssh, seriesCallback),
      seriesCallback => clearETCD2Directory(node, ssh, seriesCallback),
      seriesCallback => modifyCloudConfigFile(node, nodesList, ssh, seriesCallback),
      seriesCallback => rebootClusterNode(node, seriesCallback),
      seriesCallback => waitTillBoot(node, seriesCallback),

      // seriesCallback => sshConnect(node, ssh, seriesCallback), // reconnect
      // seriesCallback => updateNodeCoreOS(node, ssh, seriesCallback),
      // seriesCallback => rebootClusterNode(node, seriesCallback),
      // seriesCallback => waitTillBoot(node, seriesCallback),
      // seriesCallback => sshConnect(node, ssh, seriesCallback), // reconnect
      // seriesCallback => rebootClusterNode(node, seriesCallback),
    ], eachCallback);
  }, (err) => {
    callback(err, nodesList);
  });
}

async.waterfall([
  nextFunc => ensureClusterNodes(nextFunc),
  (nodesList, nextFunc) => execSSHCommands(nodesList, nextFunc),
  (nodesList, nextFunc) => initTheSwarm(nodesList, nextFunc),
], err => {
  if (err) {
    console.error('Error!!', err);
    return process.exit(1);
  }
  console.log('Done!')
  process.exit(0);
});
