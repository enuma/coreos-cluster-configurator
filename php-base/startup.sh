#!/bin/sh
echo "- Create deployer user"
adduser -H -D deployer
echo "- Ensure html dir exists"
mkdir -p /var/www/html
echo "- Change ownership to deployer"
chown -R deployer:deployer /var/www/html
echo "- Change Directory to html"
cd /var/www/html

echo "- Run php-fpm"
php-fpm
echo "- Run nginx"
nginx
echo "- Run mysql"
/usr/bin/mysqld --user=mysql --console &

echo "- Wait for mysql to bootstrap"
sleep 10s
echo "- Setting mysql root password"
/usr/bin/mysqladmin -u root password 'root'

echo "- Change User to dpeloyer"
su deployer