#Populate Key Value store if you have sth in ceph.defaults
docker run --net=host \
  -v /home/core/ceph/lib:/var/lib/ceph \
  -e KV_TYPE=etcd \
  -e KV_IP=127.0.0.1 \
  -e KV_PORT=4001 \
  ceph/daemon populate_kvstore

# run ceph monitor
docker run -d \
  --name=ceph-mon \
  --net=host \
  -v /home/core/ceph/lib:/var/lib/ceph \
  -e MON_IP=$(ip a s eth0 | grep -Po "(?<=inet ).*?(?=/17 )") \
  -e CEPH_PUBLIC_NETWORK=$(ip a s eth0 | grep -Po "(?<=inet ).*?(?=/17 )")/17 \
  -e KV_TYPE=etcd \
  -e KV_IP=127.0.0.1 \
  --entrypoint '/bin/bash' \
  ceph/daemon -c "
      [ ! -e /etc/ceph/ceph.mon.keyring ] && ceph-authtool --create-keyring /etc/ceph/ceph.mon.keyring --gen-key -n mon. --cap mon 'allow *'
      /entrypoint.sh mon
  "

# start 2 ceph osd devices
docker run -d \
  --name=ceph-osd-1 \
  --net=host \
  --pid=host \
  -v /home/core/ceph/lib:/var/lib/ceph \
  -v /dev:/dev \
  --privileged=true \
  -e OSD_TYPE=directory \
  -e OSD_FORCE_ZAP=1 \
  -e KV_TYPE=etcd \
  -e KV_IP=127.0.0.1 \
  --entrypoint '/bin/bash' \
  ceph/daemon -c "
      [ ! -e /etc/ceph/ceph.mon.keyring ] && ceph-authtool --create-keyring /etc/ceph/ceph.mon.keyring --gen-key -n mon. --cap mon 'allow *'
      /entrypoint.sh osd_ceph_disk
  "
docker run -d \
  --name=ceph-osd-2 \
  --net=host \
  --pid=host \
  -v /home/core/ceph/lib:/var/lib/ceph \
  -v /dev:/dev \
  --privileged=true \
  -e OSD_FORCE_ZAP=1 \
  -e KV_TYPE=etcd \
  -e KV_IP=127.0.0.1 \
  --entrypoint '/bin/bash' \
  ceph/daemon -c "
      [ ! -e /etc/ceph/ceph.mon.keyring ] && ceph-authtool --create-keyring /etc/ceph/ceph.mon.keyring --gen-key -n mon. --cap mon 'allow *'
      /entrypoint.sh osd_ceph_disk
  "